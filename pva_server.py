#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time
from multiprocessing import cpu_count, get_context

import numpy as np
from p4p.nt import NTNDArray
from p4p.server import Server, StaticProvider
from p4p.server.thread import SharedPV


class MyServer(object):
    def __init__(self):
        self.stop_flag = False
        self.pvdb = dict()
        self.process = None
        self.mp_ctxt = get_context("fork")

        self.provider = StaticProvider('test')

    def add_pv(self, pv_name, n_elem, prefix):
        print(pv_name)
        pv = SharedPV(nt=NTNDArray(), initial=np.zeros(n_elem))
        self.provider.add(prefix+pv_name, pv)
        self.pvdb[pv_name] = pv

    def start_server(self, freq):
        if self.pvdb != dict():
            self.process = self.mp_ctxt.Process(
                target=self._start_server, args=(freq,)
            )
            self.process.start()

    def _start_server(self, freq):
        server = Server(providers=[self.provider])
        t0 = time.time()
        sleep_time = 1 / freq
        counter = 0

        with server:
            while not self.stop_flag:
                if self.stop_flag:
                    break
                counter += 1

                for pv in self.pvdb:
                    arr = np.random.random(self.pvdb[pv].current().shape[0])
                    arr[0] = counter
                    try:
                        self.pvdb[pv].post(arr)
                    except Exception:
                        pass

                delta_t = 1 / freq - (time.time() - t0)
                t0 = time.time()
                sleep_time += delta_t
                if sleep_time > 0:
                    time.sleep(sleep_time)

    def join(self):
        if self.process is not None:
            self.process.join()


def run_server(freq, scenario, prefix):
    servers = []
    for i in range(cpu_count()):
        servers.append(MyServer())

    next_server = 0
    for filename in scenario.keys():
        n_pvs = scenario[filename]["n_pvs"]
        n_elem = scenario[filename]["n_elem"]
        for i in range(len(n_pvs)):
            for j in range(n_pvs[i]):
                pv_name = filename
                if len(n_elem) > 1:
                    pv_name += "_" + str(n_elem[i])
                if n_pvs[i] > 1:
                    pv_name += "_" + str(j)

                servers[next_server].add_pv(pv_name, n_elem[i], prefix)
                next_server += 1
                next_server %= len(servers)

    for server in servers:
        server.start_server(freq)

    for server in servers:
        server.join()


if __name__ == "__main__":
    import sys

    if len(sys.argv) >= 4:
        freq = float(sys.argv[1])
        n_elem = [
            int(float(n))
            for n in (sys.argv[2].replace("[", "").replace("]", "").split(","))
        ]
        if sys.argv[3].count("[") == 0:
            n_pvs = [int(sys.argv[3])] * len(n_elem)
        else:
            n_pvs = [
                int(n)
                for n in (sys.argv[3].replace("[", "").replace("]", "").split(","))
            ]
    else:
        freq = 14  # Hz
        n_elem = [1, 10, 100, 1000, 10000, 100000, int(1e6)]
        n_pvs = [5] * 7

    prefix = "PERF:TEST:"
    scenario = dict()
    for i in range(len(n_pvs)):
        for j in range(n_pvs[i]):
            filename = "PV_" + str(n_elem[i]) + "_" + str(j)
            scenario[filename] = dict()
            scenario[filename]["n_elem"] = [n_elem[i]]
            scenario[filename]["n_pvs"] = [1]

    data_rate = 0
    for filename in scenario.keys():
        n_pvs = scenario[filename]["n_pvs"]
        n_elem = scenario[filename]["n_elem"]
        for i in range(len(n_pvs)):
            data_rate += n_pvs[i] * n_elem[i] * 8 * 8 * freq
    print("Data rate = {0:1.2f} Mbps".format(data_rate * 1e-6))

    servers = run_server(freq, scenario, prefix)
