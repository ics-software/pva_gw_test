# Scripts for performance test of PVA gateways

This repository includes 3 scripts that can be used to do performance tests on EPICS PVA gateways. Results of such tests are stores on dedicated subfolders, which can include scripts used to run those tests.

The scripts are the following:
- pva_server.py: it generates a Python softIOC based on p4p. It uses the multiprocessing module to enable high data rates.
  
  To use it, run the following command:
  ```
  python pva_server.py  [freq] [n_elem] [n_pvs]
  ```
  - freq: the update frequency of the PVs in Hz. Default is 14 Hz.
  - n_elem: a list with the number of elements that each array will have. Default is [1, 10, 100, 1000, 10000, 100000, 1000000].
  - n_pvs: a list with the number of PVs that it should generate for each n_elem value. Default is [5, 5, 5, 5, 5, 5, 5].

  The generated PVs will have the following name: `PERF:TEST:PV_{n_elem}_{index}`, where `n_elem` is the number of elements in the array and `index` is defined starting at 0.

- client_test.py: this script monitors PVs for a limited time and then reports transmission statistics.

  Usage:
  ```
  python client_test.py [acq_time] [n_elem] [n_pvs]

- client_loop.py: it allows to run several tests in a row and generate MD tables for reports. To use it, one needs to modify the script to adapt to the test case.


---
For any information, contact Juan F. Esteban Müller JuanF.EstebanMuller@ess.eu