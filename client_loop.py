#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This script uses client_test.py to run several tests using
# different test parameters.

from p4p.client.thread import Context
import numpy as np
import time

from client_test import acquire


class Logger(object):
    def __init__(self, filename=None):
        if filename is not None:
            self.outfile = open(filename, 'w')
        self.filename = filename

    def log(self, message):
        print(message)
        if self.filename is not None:
            self.outfile.write(message+'\n')

    def close(self):
        if self.filename is not None:
            self.outfile.close()


if __name__ == "__main__":
    import sys

    freq = 14  # Hz

    if len(sys.argv) >= 4:
        acq_time = float(sys.argv[1])
        n_runs = int(sys.argv[2])
        filename = sys.argv[3]
    else:
        acq_time = 15  # seconds
        n_runs = 3
        filename = 'report'

    prefix = "PERF:TEST:"
    n_elem_a = [int(1e3), int(1e4), int(1e5)]
    n_pvs_a = [1, 5, 10, 20, 50, 100]

    test_time = acq_time * n_runs * len(n_elem_a) * len(n_pvs_a)

    # filename += '_' + time.strftime("%Y%m%d_%H%M%S", time.localtime())

    logger = Logger(filename=filename+'.md')

    ctxt = Context('pva')

    logger.log('Started: ' + time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
    logger.log('')

    # Only p4p v4
    # logger.log(ctxt.conf())
    # logger.log('')

    logger.log('Data rates tested:')
    logger.log("""
| # points | Min. Data Rate [Mbps] | Max. Data Rate [Mbps] 
| --- | --- | --- |""")
    for n_elem in n_elem_a:
        logger.log('| {0:d}k | {1:1.2f} | {2:1.2f} |'.format(n_elem//1000, np.min(n_pvs_a) *
                                                             n_elem * 8 * 8 * freq * 1e-6, np.max(n_pvs_a) * n_elem * 8 * 8 * freq * 1e-6))

    logger.log('')

    logger.log('The test will take {0:1.0f}:{1:1.0f}:{2:1.0f} s.'.format(
        test_time//3600, test_time//60 % 60, test_time % 60 % 60))

    logger.log('')

    logger.log("""
| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |""")

    data = dict()

    for n_elem in n_elem_a:
        for n_pvs in n_pvs_a:
            scenario = dict()
            for i in range(n_pvs):
                pvname = "PV_" + str(n_elem) + "_" + str(i)
                scenario[pvname] = dict()
                scenario[pvname]["n_elem"] = [n_elem]
                scenario[pvname]["n_pvs"] = [1]

            data_rate = n_pvs * n_elem * 8 * 8 * freq

            data[data_rate] = []

            for i in range(n_runs):
                client = acquire(scenario, prefix, acq_time)

                try:
                    pv, sent_values,  received_values, transmission, freq_rec, freq_gen = client.report()
                except:
                    pv, sent_values,  received_values, transmission, freq_rec, freq_gen = [
                        0, 0, 0, 0, 0, 0]

                if i == 0:
                    logger.log("| {0:.0f}k | {1:d} | {2:1.2f} | {3:d} | {4:1.2f} | {5:1.2f} |".format(n_elem/1e3, n_pvs, data_rate *
                                                                                                      1e-6, i+1, 1e-6*np.sum(np.array(freq_rec)*8*8*n_elem), 100*np.sum(received_values)/np.sum(sent_values)))
                else:
                    logger.log("|   |   |   | {0:d} | {1:1.2f} | {2:1.2f} |".format(
                        i+1, 1e-6*np.sum(np.array(freq_rec)*8*8*n_elem), 100*np.sum(received_values)/np.sum(sent_values)))

                data[data_rate].append(
                    100*np.sum(received_values)/np.sum(sent_values))

    logger.close()

    outf = open(filename+'.txt', 'w')
    for d in data:
        outf.write('{0:.0f}'.format(d))
        outf.write('\t')
        outf.write(str(data[d]))
        outf.write('\n')
    outf.close()
