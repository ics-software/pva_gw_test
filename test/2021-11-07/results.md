# Performance test of PVA gateways - 2021-11-07
This report describes a simple performance test that has been done to evaluate the latest alpha release of the PVA gateway, which uses p4p v4.0.2a. 
The major change on this p4p version is that it uses the PVXS libraries instead of pvAccessCPP.

At ESS we are interested in this new version because it solves an issue when chaining gateways https://github.com/mdavidsaver/p4p/issues/53, which is the setup we are using.

## Setup
The setup consists of two virtual machines on the CSLab network, both with p4p v3.5.3 installed.
- One machine runs a Python IOC that generates a set of PVs with different array lengths. All PVs contain a counter plus random generated numbers that are updated at 14 Hz. This machine has 8 GB of RAM and 4 cores.
- The other machine runs a client that monitors a number of PVs for a time interval and then checks the counters to see how much data it received. This machine has 8 GB of RAM and 8 cores.

While the tests are running, the scalar PV is retrieved from another machine (my laptop), to check the effect of a high load in the gateway on the delay to get the data.

## Tests
The server is run using the following command:
```
[server] $ python pva_server.py 14 "[1,1000,10000,100000]" "[1,100,100,100]"
```
which generates 1 scalar PV plus 3 sets of 100 PVs with arrays of length 1 000, 10 000, and 100 000 points. The update rate is 14 Hz.

The client tests several configurations monitoring during 15 seconds a subset of those PVs, always of the same length, but varying the number of them from 1, 5, 10, 20, 50 and 100. Each test case is repeated 3 times. The parameter space is shown on the following table.
| # points | Min. Data Rate [Mbps] | Max. Data Rate [Mbps] |
| --- | --- | --- |
| 1000 | 0.90 | 44.80 |
| 10000 | 8.96 | 448.00 |
| 100000 | 89.60 | 4480.00 |

Tests are run in 3 different scenarios:
- Connecting directly from the client to the server.
- Using the PVA gateway running p4p v4
- Using the PVA gateway running p4p v3.5

### Direct connection between client and server
A test connecting directly the client and the server is run to verify what can be achieved without a gateway.

The client runs with the following command:
```
[client] $ python client_loop.py
```

Transmission is 100% for all cases except for the highest load monitoring 100 PVs with 100k points, which brought transmission down to ~80%. Results are summarized in the following table:
| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |
| 1k | 1 | 0.90 | 1 | 0.90 | 100.00 |
|   |   |   | 2 | 0.90 | 100.00 |
|   |   |   | 3 | 0.90 | 100.00 |
| 1k | 5 | 4.48 | 1 | 4.49 | 100.00 |
|   |   |   | 2 | 4.50 | 100.00 |
|   |   |   | 3 | 4.49 | 100.00 |
| 1k | 10 | 8.96 | 1 | 8.98 | 100.00 |
|   |   |   | 2 | 8.98 | 100.00 |
|   |   |   | 3 | 8.98 | 100.00 |
| 1k | 20 | 17.92 | 1 | 17.96 | 100.00 |
|   |   |   | 2 | 17.98 | 100.00 |
|   |   |   | 3 | 17.95 | 100.00 |
| 1k | 50 | 44.80 | 1 | 44.02 | 100.00 |
|   |   |   | 2 | 44.90 | 100.00 |
|   |   |   | 3 | 42.26 | 100.00 |
| 1k | 100 | 89.60 | 1 | 89.94 | 100.00 |
|   |   |   | 2 | 87.14 | 100.00 |
|   |   |   | 3 | 88.04 | 100.00 |
| 10k | 1 | 8.96 | 1 | 8.98 | 100.00 |
|   |   |   | 2 | 8.99 | 100.00 |
|   |   |   | 3 | 9.00 | 100.00 |
| 10k | 5 | 44.80 | 1 | 44.87 | 100.00 |
|   |   |   | 2 | 44.88 | 100.00 |
|   |   |   | 3 | 44.92 | 100.00 |
| 10k | 10 | 89.60 | 1 | 89.85 | 100.00 |
|   |   |   | 2 | 89.96 | 100.00 |
|   |   |   | 3 | 89.77 | 100.00 |
| 10k | 20 | 179.20 | 1 | 179.60 | 100.00 |
|   |   |   | 2 | 179.96 | 100.00 |
|   |   |   | 3 | 179.61 | 100.00 |
| 10k | 50 | 448.00 | 1 | 449.33 | 100.00 |
|   |   |   | 2 | 450.35 | 100.00 |
|   |   |   | 3 | 450.74 | 100.00 |
| 10k | 100 | 896.00 | 1 | 899.88 | 100.00 |
|   |   |   | 2 | 897.87 | 100.00 |
|   |   |   | 3 | 898.08 | 100.00 |
| 100k | 1 | 89.60 | 1 | 89.72 | 100.00 |
|   |   |   | 2 | 89.84 | 100.00 |
|   |   |   | 3 | 89.94 | 100.00 |
| 100k | 5 | 448.00 | 1 | 448.66 | 100.00 |
|   |   |   | 2 | 448.71 | 100.00 |
|   |   |   | 3 | 449.24 | 100.00 |
| 100k | 10 | 896.00 | 1 | 898.75 | 100.00 |
|   |   |   | 2 | 897.28 | 100.00 |
|   |   |   | 3 | 898.32 | 100.00 |
| 100k | 20 | 1792.00 | 1 | 1795.77 | 100.00 |
|   |   |   | 2 | 1796.31 | 100.00 |
|   |   |   | 3 | 1796.86 | 100.00 |
| 100k | 50 | 4480.00 | 1 | 4494.55 | 100.00 |
|   |   |   | 2 | 4505.22 | 100.00 |
|   |   |   | 3 | 4487.73 | 100.00 |
| 100k | 100 | 8960.00 | 1 | 7269.37 | 82.81 |
|   |   |   | 2 | 6892.27 | 78.43 |
|   |   |   | 3 | 7168.44 | 81.57 |

### PVA GW p4p v4
The same test is repeated now passing through the PVA gateway based on p4p v4 with the following command:
```
[client] $ EPICS_PVA_ADDR_LIST="172.30.9.12" EPICS_PVA_AUTO_ADDR_LIST=NO python client_loop.py
```

Results are slightly worse than the case without a gateway, but almost the same. Transmission is slightly degraded in one of the 3 runs of the test case that monitors 50 PVs with 100k points (99.99%) and it goes down to 55-60% in the case with the highest load.

In addition, the delay acquiring a scalar PV goes from an average of 47 ms over 10 acquisitions to 410 ms.

| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |
| 1k | 1 | 0.90 | 1 | 0.90 | 100.00 |
|   |   | 0.90 | 2 | 0.90 | 100.00 |
|   |   | 0.90 | 3 | 0.90 | 100.00 |
| 1k | 5 | 4.48 | 1 | 4.49 | 100.00 |
|   |   | 4.48 | 2 | 4.48 | 100.00 |
|   |   | 4.48 | 3 | 4.48 | 100.00 |
| 1k | 10 | 8.96 | 1 | 8.99 | 100.00 |
|   |   | 8.96 | 2 | 8.98 | 100.00 |
|   |   | 8.96 | 3 | 8.99 | 100.00 |
| 1k | 20 | 17.92 | 1 | 17.97 | 100.00 |
|   |   | 17.92 | 2 | 17.92 | 100.00 |
|   |   | 17.92 | 3 | 17.96 | 100.00 |
| 1k | 50 | 44.80 | 1 | 44.92 | 100.00 |
|   |   | 44.80 | 2 | 44.84 | 100.00 |
|   |   | 44.80 | 3 | 44.93 | 100.00 |
| 1k | 100 | 89.60 | 1 | 89.93 | 100.00 |
|   |   | 89.60 | 2 | 89.89 | 100.00 |
|   |   | 89.60 | 3 | 90.10 | 100.00 |
| 10k | 1 | 8.96 | 1 | 8.99 | 100.00 |
|   |   | 8.96 | 2 | 8.99 | 100.00 |
|   |   | 8.96 | 3 | 9.00 | 100.00 |
| 10k | 5 | 44.80 | 1 | 44.89 | 100.00 |
|   |   | 44.80 | 2 | 44.82 | 100.00 |
|   |   | 44.80 | 3 | 44.84 | 100.00 |
| 10k | 10 | 89.60 | 1 | 89.90 | 100.00 |
|   |   | 89.60 | 2 | 89.84 | 100.00 |
|   |   | 89.60 | 3 | 89.59 | 100.00 |
| 10k | 20 | 179.20 | 1 | 179.57 | 100.00 |
|   |   | 179.20 | 2 | 179.63 | 100.00 |
|   |   | 179.20 | 3 | 179.47 | 100.00 |
| 10k | 50 | 448.00 | 1 | 448.60 | 100.00 |
|   |   | 448.00 | 2 | 449.51 | 100.00 |
|   |   | 448.00 | 3 | 449.33 | 100.00 |
| 10k | 100 | 896.00 | 1 | 898.24 | 100.00 |
|   |   | 896.00 | 2 | 898.57 | 100.00 |
|   |   | 896.00 | 3 | 899.22 | 100.00 |
| 100k | 1 | 89.60 | 1 | 89.97 | 100.00 |
|   |   | 89.60 | 2 | 89.72 | 100.00 |
|   |   | 89.60 | 3 | 89.79 | 100.00 |
| 100k | 5 | 448.00 | 1 | 449.03 | 100.00 |
|   |   | 448.00 | 2 | 448.71 | 100.00 |
|   |   | 448.00 | 3 | 448.76 | 100.00 |
| 100k | 10 | 896.00 | 1 | 899.20 | 100.00 |
|   |   | 896.00 | 2 | 896.50 | 100.00 |
|   |   | 896.00 | 3 | 897.13 | 100.00 |
| 100k | 20 | 1792.00 | 1 | 1797.59 | 100.00 |
|   |   | 1792.00 | 2 | 1796.75 | 100.00 |
|   |   | 1792.00 | 3 | 1799.50 | 100.00 |
| 100k | 50 | 4480.00 | 1 | 4491.39 | 100.00 |
|   |   | 4480.00 | 2 | 4494.10 | 100.00 |
|   |   | 4480.00 | 3 | 4522.27 | 99.99 |
| 100k | 100 | 8960.00 | 1 | 4959.81 | 61.44 |
|   |   | 8960.00 | 2 | 4442.61 | 61.60 |
|   |   | 8960.00 | 3 | 4395.98 | 55.36 |

The following plot shows the load on the gateway, where one can see that the resources seem to be sufficient.
![](pva_gw_p4p_v4.png)

## PVA GW p4p v3
Finally, the same test is repeated again using the PVA gateway based on p4p v3.4.2 with the following command:

```
[client] $ EPICS_PVA_ADDR_LIST="172.30.9.11" EPICS_PVA_AUTO_ADDR_LIST=NO python client_loop.py
```

Results are a bit worse than for the gateway based on p4p v4. Transmission goes below 85% for one of the 3 runs of the test case that monitors 50 PVs with 100k points and to ~45% in the case with the highest load.

The delay acquiring a scalar PV goes from an average of 50 ms over 10 acquisitions to 239 ms.

| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |
| 1k | 1 | 0.90 | 1 | 0.90 | 100.00 |
|   |   |   | 2 | 0.90 | 100.00 |
|   |   |   | 3 | 0.90 | 100.00 |
| 1k | 5 | 4.48 | 1 | 4.49 | 100.00 |
|   |   |   | 2 | 4.49 | 100.00 |
|   |   |   | 3 | 4.49 | 100.00 |
| 1k | 10 | 8.96 | 1 | 8.98 | 100.00 |
|   |   |   | 2 | 8.99 | 100.00 |
|   |   |   | 3 | 8.99 | 100.00 |
| 1k | 20 | 17.92 | 1 | 17.95 | 100.00 |
|   |   |   | 2 | 17.98 | 100.00 |
|   |   |   | 3 | 17.97 | 100.00 |
| 1k | 50 | 44.80 | 1 | 44.95 | 100.00 |
|   |   |   | 2 | 44.94 | 100.00 |
|   |   |   | 3 | 44.93 | 100.00 |
| 1k | 100 | 89.60 | 1 | 89.78 | 100.00 |
|   |   |   | 2 | 89.89 | 100.00 |
|   |   |   | 3 | 89.93 | 100.00 |
| 10k | 1 | 8.96 | 1 | 8.98 | 100.00 |
|   |   |   | 2 | 8.99 | 100.00 |
|   |   |   | 3 | 9.00 | 100.00 |
| 10k | 5 | 44.80 | 1 | 44.89 | 100.00 |
|   |   |   | 2 | 44.88 | 100.00 |
|   |   |   | 3 | 44.90 | 100.00 |
| 10k | 10 | 89.60 | 1 | 89.74 | 100.00 |
|   |   |   | 2 | 89.80 | 100.00 |
|   |   |   | 3 | 89.93 | 100.00 |
| 10k | 20 | 179.20 | 1 | 179.61 | 100.00 |
|   |   |   | 2 | 179.88 | 100.00 |
|   |   |   | 3 | 179.53 | 100.00 |
| 10k | 50 | 448.00 | 1 | 449.46 | 100.00 |
|   |   |   | 2 | 450.13 | 100.00 |
|   |   |   | 3 | 449.67 | 100.00 |
| 10k | 100 | 896.00 | 1 | 897.35 | 100.00 |
|   |   |   | 2 | 898.88 | 100.00 |
|   |   |   | 3 | 900.35 | 100.00 |
| 100k | 1 | 89.60 | 1 | 89.73 | 100.00 |
|   |   |   | 2 | 89.77 | 100.00 |
|   |   |   | 3 | 89.76 | 100.00 |
| 100k | 5 | 448.00 | 1 | 450.30 | 100.00 |
|   |   |   | 2 | 448.58 | 100.00 |
|   |   |   | 3 | 449.21 | 100.00 |
| 100k | 10 | 896.00 | 1 | 898.31 | 100.00 |
|   |   |   | 2 | 897.12 | 100.00 |
|   |   |   | 3 | 899.14 | 100.00 |
| 100k | 20 | 1792.00 | 1 | 1796.04 | 100.00 |
|   |   |   | 2 | 1796.57 | 100.00 |
|   |   |   | 3 | 1796.88 | 100.00 |
| 100k | 50 | 4480.00 | 1 | 4264.32 | 97.14 |
|   |   |   | 2 | 4196.94 | 95.17 |
|   |   |   | 3 | 3634.06 | 84.75 |
| 100k | 100 | 8960.00 | 1 | 3274.99 | 41.39 |
|   |   |   | 2 | 3707.51 | 46.19 |
|   |   |   | 3 | 3524.27 | 43.42 |

The following plot shows the load on the gateway, where one can see that the resources seem to be sufficient.
![](pva_gw_p4p_v3.png)

## Comparison
The gateway based on p4p v4 gives a better transmission for high load than the one based on p4p v3.5, but at that high load the delay to acquire scalar PVs is almost double.

The following plot shows a comparison of the transmission for the different scenarios:
![](comparison.png)


## Next steps
The next step required to validate this new gateway is to run similar tests with 2 chained gateways. This is the setup used at ESS to aggregate data from all the different gateways in the technical network and provide read-only access to PVs from the general purpose network.
