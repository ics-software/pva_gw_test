import numpy as np

from client_test import acquire

acq_time = 15  # seconds
freq = 14  # Hz
n_runs = 3

prefix = "PERF:TEST:"
n_elem_a = [int(1e3), int(1e4), int(1e5)]
n_pvs_a = [1, 5, 10, 20, 50, 100]

test_time = acq_time * n_runs * len(n_elem_a) * len(n_pvs_a)

print('Data rates tested:')
print("""
| # points | Min. Data Rate [Mbps] | Max. Data Rate [Mbps] |
| --- | --- | --- |""")
for n_elem in n_elem_a:
    print('| {0:d}k | {1:1.2f} | {2:1.2f} |'.format(n_elem//1000, np.min(n_pvs_a) * n_elem * 8 * 8 * freq * 1e-6, np.max(n_pvs_a) * n_elem * 8 * 8 * freq * 1e-6))

print('The test will take {0:d}:{1:d}:{2:d} s.'.format(test_time//3600, test_time//60%60, test_time%60%60))

print("""
| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |""")

for n_elem in n_elem_a:
    for n_pvs in n_pvs_a:
        scenario = dict()
        for i in range(n_pvs):
            filename = "PV_" + str(n_elem) + "_" + str(i)
            scenario[filename] = dict()
            scenario[filename]["n_elem"] = [n_elem]
            scenario[filename]["n_pvs"] = [1]

        data_rate = n_pvs * n_elem * 8 * 8 * freq

        for i in range(n_runs):
            client = acquire(scenario, prefix, acq_time)

            pv, sent_values,  received_values, transmission, freq_rec, freq_gen = client.report()

            if i==0:
                print("| {0:.0f}k | {1:d} | {2:1.2f} | {3:d} | {4:1.2f} | {5:1.2f} |".format(n_elem/1e3, n_pvs, data_rate * 1e-6, i+1, 1e-6*np.sum(np.array(freq_rec)*8*8*n_elem), 100*np.sum(received_values)/np.sum(sent_values)))
            else:
                print("|   |   |   | {0:d} | {1:1.2f} | {2:1.2f} |".format(i+1, 1e-6*np.sum(np.array(freq_rec)*8*8*n_elem), 100*np.sum(received_values)/np.sum(sent_values)))

            # print('freq_rec = {0:1.2f} Hz [{1:1.2f}--{2:1.2f}]'.format(
            #     np.mean(freq_rec), np.min(freq_rec), np.max(freq_rec)))
            # print('freq_gen = {0:1.2f} Hz [{1:1.2f}--{2:1.2f}]'.format(
            #     np.mean(freq_gen), np.min(freq_gen), np.max(freq_gen)))
