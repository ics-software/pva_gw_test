import numpy as np
import matplotlib.pyplot as plt

no_gw_dr = np.array([0.90, 4.48, 8.96, 17.92, 44.80, 89.60, 8.96,44.80 , 89.60, 179.20, 448.00, 896.00, 89.60, 448.00, 896.00, 1792.00, 4480.00,8960.00])

no_gw_tx = np.array([[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[82.81, 78.43, 81.57]])


p4p_v4_dr = np.array([0.90, 4.48, 8.96, 17.92, 44.80, 89.60, 8.96,44.80 , 89.60, 179.20, 448.00, 896.00, 89.60, 448.00, 896.00, 1792.00, 4480.00,8960.00])

p4p_v4_tx = np.array([[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 99.99], 
[61.44, 61.60 , 55.36]])

p4p_v3_dr = np.array([0.90, 4.48, 8.96, 17.92, 44.80, 89.60, 8.96,44.80 , 89.60, 179.20, 448.00, 896.00, 89.60, 448.00, 896.00, 1792.00, 4480.00,8960.00])

p4p_v3_tx = np.array([[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[100.00, 100.00, 100.00], 
[97.14,  95.17, 84.75], 
[41.39, 46.19, 43.42]])

def plot_errorbar(x,y, label=''):
    y_avg = np.mean(y,1)
    y_min_delta = y_avg - np.min(y,1)
    y_max_delta = np.max(y,1)- y_avg

    y_avg = y_avg[x.argsort()]
    y_min_delta = y_min_delta[x.argsort()]
    y_max_delta = y_max_delta[x.argsort()]
    x.sort()

    plt.errorbar(x, y_avg, yerr=[y_min_delta, y_max_delta], fmt='.-', capsize=3, label=label)

plt.figure()
plot_errorbar(no_gw_dr, no_gw_tx, label='No GW')
plot_errorbar(p4p_v4_dr, p4p_v4_tx, label='GW p4p v4')
plot_errorbar(p4p_v3_dr, p4p_v3_tx, label='GW p4p v3')
# plt.xscale('log')
plt.xlabel('Data rate at IOC [Mbps]')
plt.ylabel('Transmission [%]')
plt.legend(loc=0)
# plt.show()

plt.savefig('comparison.png')