#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This script runs a single test where several PVs 
# are acquired during a period time.

import sys
import time

import numpy as np
from p4p.client.thread import Context

ctxt = Context('pva')


class Monitor(object):
    def __init__(self, pv_name):
        self.pv_name = pv_name
        self.ts_gen = []
        self.ts_rec = []
        self.values = []
        self.sub = ctxt.monitor(pv_name, self.callback)

    def callback(self, value):
        self.values.append(value[0])
        self.ts_gen.append(value.raw_stamp[0] + value.raw_stamp[1] * 1e-9)
        self.ts_rec.append(time.time())

    def close(self):
        try:
            self.sub.close()
        except Exception:
            pass


class Client(object):
    def __init__(self):
        self.stop_flag = False
        self.monitors = []

    def add_pv(self, pv_name, prefix):
        monitor = Monitor(prefix+pv_name)
        self.monitors.append(monitor)

    def stop_acq(self):
        for monitor in self.monitors:
            monitor.close()
        ctxt.disconnect()

    def report(self):
        pv = []
        sent_values = []
        received_values = []
        transmission = []
        freq = []
        freq_gen = []
        for monitor in self.monitors:
            if monitor.values != [] and monitor.values[0] is not None and monitor.values[-1] is not None:
                sent_values.append(
                    (monitor.values[-1] - monitor.values[0] + 1))
                received_values.append(len(monitor.values))
                transmission.append(
                    len(monitor.values) / (monitor.values[-1] - monitor.values[0] + 1))
                freq.append((len(monitor.values) - 1) /
                            (monitor.ts_rec[-1] - monitor.ts_rec[0]))
                freq_gen.append((monitor.values[-1] - monitor.values[0]) /
                                (monitor.ts_gen[-1] - monitor.ts_gen[0]))
                pv.append(monitor.pv_name)

        return pv, sent_values,  received_values, transmission, freq, freq_gen


def acquire(scenario, prefix, acq_time):
    client = Client()

    for filename in scenario.keys():
        n_pvs = scenario[filename]["n_pvs"]
        n_elem = scenario[filename]["n_elem"]
        for i in range(len(n_pvs)):
            for j in range(n_pvs[i]):
                pv_name = filename
                if len(n_elem) > 1:
                    pv_name += "_" + str(n_elem[i])
                if n_pvs[i] > 1:
                    pv_name += "_" + str(j)

                client.add_pv(pv_name, prefix)

    time.sleep(acq_time)

    client.stop_acq()

    return client


if __name__ == "__main__":
    import sys

    freq = 14  # Hz

    if len(sys.argv) >= 4:
        acq_time = float(sys.argv[1])
        n_elem = [
            int(float(n))
            for n in (sys.argv[2].replace("[", "").replace("]", "").split(","))
        ]
        if sys.argv[3].count("[") == 0:
            n_pvs = [int(sys.argv[3])] * len(n_elem)
        else:
            n_pvs = [
                int(n)
                for n in (sys.argv[3].replace("[", "").replace("]", "").split(","))
            ]
    else:
        n_elem = [1, 10, 100, 1000, 10000, 100000, int(1e6)]
        n_pvs = [5] * 7
        acq_time = 15  # seconds

    prefix = "PERF:TEST:"
    scenario = dict()
    for i in range(len(n_pvs)):
        for j in range(n_pvs[i]):
            filename = "PV_" + str(n_elem[i]) + "_" + str(j)
            scenario[filename] = dict()
            scenario[filename]["n_elem"] = [n_elem[i]]
            scenario[filename]["n_pvs"] = [1]

    data_rate = 0
    for filename in scenario.keys():
        n_pvs = scenario[filename]["n_pvs"]
        n_elem = scenario[filename]["n_elem"]
        for i in range(len(n_pvs)):
            data_rate += n_pvs[i] * n_elem[i] * 8 * 8 * freq
    print("Data rate = {0:1.2f} Mbps".format(data_rate * 1e-6))

    client = acquire(scenario, prefix, acq_time)

    pv, sent_values,  received_values, transmission, freq, freq_gen = client.report()

    print('Total transmission = {0:1.2f}%'.format(
        100*np.sum(received_values)/np.sum(sent_values)))
    print('Effective data rate = {0:1.2f} Mbps'.format(
        1e-6*np.sum(np.array(freq)*8*8*n_elem[0])))
    print('freq_rec = {0:1.2f} Hz [{1:1.2f}--{2:1.2f}]'.format(
        np.mean(freq), np.min(freq), np.max(freq)))
    print('freq_gen = {0:1.2f} Hz [{1:1.2f}--{2:1.2f}]'.format(
        np.mean(freq_gen), np.min(freq_gen), np.max(freq_gen)))
