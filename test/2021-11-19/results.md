# Performance test of PVA gateways - 2021-11-19
This is a repetition of the tests done on 2021-11-07. See the report [here](../2021-11-07/results.md).

The main difference is that now we made sure all virtual machines run on different hosts, so that we ensure all traffic goes throught the network. In the previous case, the gateway based on p4p v3 was running on the same machine as IOC.

A new gateway, `epics-gw-lab-03.cslab.esss.lu.se`, has been deployed. It runs p4p v4.0.2a and it is configured to point to the other p4p v4 gateway, `epics-gw-lab-02.cslab.esss.lu.se`, to emulate the usual setup we have at ESS where in most cases we use chained gateways.

Also, in the meantime the CSLab cluster has been upgraded with more resources.

## Running the tests
A new script, `gateway_test.py` was prepared to run all the tests sequencially. The tests are run in the following order:
- No gateway: direct connection between client and IOC.
- `epics-gw-lab-01.cslab.esss.lu.se`: the gateway running on p4p v3.4.2
- `epics-gw-lab-02.cslab.esss.lu.se`: the gateway running on p4p v4.0.2a
- `epics-gw-lab-03.cslab.esss.lu.se`: the chained gateway running on p4p v4.0.2a

The acquisition time was also increased from 15s to 30s per PV.

### Direct connection between client and server
A test connecting directly the client and the server is run to verify what can be achieved without a gateway.

Transmission is 100% for all cases. The improvement with respect of previous results might be related to the upgrade of the CSLab cluster.

Results are summarized in the following table:
| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |
| 1k | 1 | 0.90 | 1 | 0.90 | 100.00 |
|   |   |   | 2 | 0.90 | 100.00 |
|   |   |   | 3 | 0.90 | 100.00 |
| 1k | 5 | 4.48 | 1 | 4.49 | 100.00 |
|   |   |   | 2 | 4.49 | 100.00 |
|   |   |   | 3 | 4.49 | 100.00 |
| 1k | 10 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.97 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 1k | 20 | 17.92 | 1 | 17.94 | 100.00 |
|   |   |   | 2 | 17.94 | 100.00 |
|   |   |   | 3 | 17.95 | 100.00 |
| 1k | 50 | 44.80 | 1 | 44.89 | 100.00 |
|   |   |   | 2 | 44.88 | 100.00 |
|   |   |   | 3 | 44.85 | 100.00 |
| 1k | 100 | 89.60 | 1 | 89.69 | 100.00 |
|   |   |   | 2 | 89.74 | 100.00 |
|   |   |   | 3 | 89.71 | 100.00 |
| 10k | 1 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.97 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 10k | 5 | 44.80 | 1 | 44.84 | 100.00 |
|   |   |   | 2 | 44.84 | 100.00 |
|   |   |   | 3 | 44.85 | 100.00 |
| 10k | 10 | 89.60 | 1 | 89.72 | 100.00 |
|   |   |   | 2 | 89.69 | 100.00 |
|   |   |   | 3 | 89.69 | 100.00 |
| 10k | 20 | 179.20 | 1 | 179.36 | 100.00 |
|   |   |   | 2 | 179.40 | 100.00 |
|   |   |   | 3 | 179.39 | 100.00 |
| 10k | 50 | 448.00 | 1 | 448.47 | 100.00 |
|   |   |   | 2 | 448.47 | 100.00 |
|   |   |   | 3 | 448.47 | 100.00 |
| 10k | 100 | 896.00 | 1 | 897.35 | 100.00 |
|   |   |   | 2 | 896.85 | 100.00 |
|   |   |   | 3 | 897.26 | 100.00 |
| 100k | 1 | 89.60 | 1 | 89.68 | 100.00 |
|   |   |   | 2 | 89.69 | 100.00 |
|   |   |   | 3 | 89.69 | 100.00 |
| 100k | 5 | 448.00 | 1 | 448.59 | 100.00 |
|   |   |   | 2 | 448.43 | 100.00 |
|   |   |   | 3 | 448.40 | 100.00 |
| 100k | 10 | 896.00 | 1 | 896.91 | 100.00 |
|   |   |   | 2 | 896.95 | 100.00 |
|   |   |   | 3 | 896.73 | 100.00 |
| 100k | 20 | 1792.00 | 1 | 1795.61 | 100.00 |
|   |   |   | 2 | 1794.47 | 100.00 |
|   |   |   | 3 | 1793.35 | 100.00 |
| 100k | 50 | 4480.00 | 1 | 4483.26 | 100.00 |
|   |   |   | 2 | 4484.15 | 100.00 |
|   |   |   | 3 | 4484.07 | 100.00 |
| 100k | 100 | 8960.00 | 1 | 8971.56 | 100.00 |
|   |   |   | 2 | 8971.91 | 100.00 |
|   |   |   | 3 | 8972.45 | 100.00 |

### PVA GW p4p v3
The same test is repeated now passing through the PVA gateway based on p4p v3.

Results are slightly worse than the case without a gateway, with transmission degraded in the case with the highest load, when it went to around 70%.

| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |
| 1k | 1 | 0.90 | 1 | 0.90 | 100.00 |
|   |   |   | 2 | 0.90 | 100.00 |
|   |   |   | 3 | 0.90 | 100.00 |
| 1k | 5 | 4.48 | 1 | 4.49 | 100.00 |
|   |   |   | 2 | 4.48 | 100.00 |
|   |   |   | 3 | 4.49 | 100.00 |
| 1k | 10 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.98 | 100.00 |
|   |   |   | 3 | 8.98 | 100.00 |
| 1k | 20 | 17.92 | 1 | 17.94 | 100.00 |
|   |   |   | 2 | 17.93 | 100.00 |
|   |   |   | 3 | 17.94 | 100.00 |
| 1k | 50 | 44.80 | 1 | 44.86 | 100.00 |
|   |   |   | 2 | 44.86 | 100.00 |
|   |   |   | 3 | 44.89 | 100.00 |
| 1k | 100 | 89.60 | 1 | 89.72 | 100.00 |
|   |   |   | 2 | 89.84 | 100.00 |
|   |   |   | 3 | 89.69 | 100.00 |
| 10k | 1 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.98 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 10k | 5 | 44.80 | 1 | 44.88 | 100.00 |
|   |   |   | 2 | 44.85 | 100.00 |
|   |   |   | 3 | 44.87 | 100.00 |
| 10k | 10 | 89.60 | 1 | 89.72 | 100.00 |
|   |   |   | 2 | 89.78 | 100.00 |
|   |   |   | 3 | 89.71 | 100.00 |
| 10k | 20 | 179.20 | 1 | 179.31 | 100.00 |
|   |   |   | 2 | 179.47 | 100.00 |
|   |   |   | 3 | 179.40 | 100.00 |
| 10k | 50 | 448.00 | 1 | 449.02 | 100.00 |
|   |   |   | 2 | 448.87 | 100.00 |
|   |   |   | 3 | 449.09 | 100.00 |
| 10k | 100 | 896.00 | 1 | 896.76 | 100.00 |
|   |   |   | 2 | 897.34 | 100.00 |
|   |   |   | 3 | 897.30 | 100.00 |
| 100k | 1 | 89.60 | 1 | 89.73 | 100.00 |
|   |   |   | 2 | 89.69 | 100.00 |
|   |   |   | 3 | 89.71 | 100.00 |
| 100k | 5 | 448.00 | 1 | 448.74 | 100.00 |
|   |   |   | 2 | 448.74 | 100.00 |
|   |   |   | 3 | 448.85 | 100.00 |
| 100k | 10 | 896.00 | 1 | 896.93 | 100.00 |
|   |   |   | 2 | 896.86 | 100.00 |
|   |   |   | 3 | 896.98 | 100.00 |
| 100k | 20 | 1792.00 | 1 | 1794.26 | 100.00 |
|   |   |   | 2 | 1794.63 | 100.00 |
|   |   |   | 3 | 1794.62 | 100.00 |
| 100k | 50 | 4480.00 | 1 | 4490.27 | 100.00 |
|   |   |   | 2 | 4500.65 | 100.00 |
|   |   |   | 3 | 4496.35 | 100.00 |
| 100k | 100 | 8960.00 | 1 | 5853.99 | 67.57 |
|   |   |   | 2 | 6222.48 | 70.96 |
|   |   |   | 3 | 6217.93 | 70.71 |

The following plot shows the load on the gateway, where one can see that the resources seem to be sufficient.
![](pva_gw_p4p_v3.png)

## PVA GW p4p v4
The same test is repeated again using the PVA gateway based on p4p v4.0.2a.

Results are similar to those for the gateway based on p4p v3. Transmission goes to around 60% for the case with the highest load.

| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |
| 1k | 1 | 0.90 | 1 | 0.90 | 100.00 |
|   |   |   | 2 | 0.90 | 100.00 |
|   |   |   | 3 | 0.90 | 100.00 |
| 1k | 5 | 4.48 | 1 | 4.48 | 100.00 |
|   |   |   | 2 | 4.49 | 100.00 |
|   |   |   | 3 | 4.48 | 100.00 |
| 1k | 10 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.97 | 100.00 |
|   |   |   | 3 | 8.98 | 100.00 |
| 1k | 20 | 17.92 | 1 | 17.95 | 100.00 |
|   |   |   | 2 | 17.94 | 100.00 |
|   |   |   | 3 | 17.96 | 100.00 |
| 1k | 50 | 44.80 | 1 | 44.88 | 100.00 |
|   |   |   | 2 | 44.87 | 100.00 |
|   |   |   | 3 | 44.90 | 100.00 |
| 1k | 100 | 89.60 | 1 | 89.74 | 100.00 |
|   |   |   | 2 | 89.72 | 100.00 |
|   |   |   | 3 | 89.86 | 100.00 |
| 10k | 1 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.97 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 10k | 5 | 44.80 | 1 | 44.84 | 100.00 |
|   |   |   | 2 | 44.89 | 100.00 |
|   |   |   | 3 | 44.84 | 100.00 |
| 10k | 10 | 89.60 | 1 | 89.73 | 100.00 |
|   |   |   | 2 | 89.68 | 100.00 |
|   |   |   | 3 | 89.68 | 100.00 |
| 10k | 20 | 179.20 | 1 | 179.43 | 100.00 |
|   |   |   | 2 | 179.38 | 100.00 |
|   |   |   | 3 | 179.36 | 100.00 |
| 10k | 50 | 448.00 | 1 | 448.50 | 100.00 |
|   |   |   | 2 | 448.43 | 100.00 |
|   |   |   | 3 | 448.48 | 100.00 |
| 10k | 100 | 896.00 | 1 | 897.07 | 100.00 |
|   |   |   | 2 | 897.15 | 100.00 |
|   |   |   | 3 | 897.53 | 100.00 |
| 100k | 1 | 89.60 | 1 | 89.69 | 100.00 |
|   |   |   | 2 | 89.85 | 100.00 |
|   |   |   | 3 | 89.68 | 100.00 |
| 100k | 5 | 448.00 | 1 | 448.32 | 100.00 |
|   |   |   | 2 | 448.90 | 100.00 |
|   |   |   | 3 | 448.46 | 100.00 |
| 100k | 10 | 896.00 | 1 | 897.12 | 100.00 |
|   |   |   | 2 | 896.73 | 100.00 |
|   |   |   | 3 | 897.67 | 100.00 |
| 100k | 20 | 1792.00 | 1 | 1793.89 | 100.00 |
|   |   |   | 2 | 1793.09 | 100.00 |
|   |   |   | 3 | 1794.85 | 100.00 |
| 100k | 50 | 4480.00 | 1 | 4484.06 | 100.00 |
|   |   |   | 2 | 4485.80 | 100.00 |
|   |   |   | 3 | 4493.31 | 100.00 |
| 100k | 100 | 8960.00 | 1 | 5081.53 | 58.77 |
|   |   |   | 2 | 4717.88 | 53.96 |
|   |   |   | 3 | 5609.51 | 63.56 |

The following plot shows the load on the gateway, where one can see that the resources seem to be sufficient.
![](pva_gw_p4p_v4.png)

## Chained gateways
The final test case uses the chained gateways based on p4p v4.0.2a.

Again, results are similar to the previous ones, with small degradation for the case with the highest load.

| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |
| 1k | 1 | 0.90 | 1 | 0.90 | 100.00 |
|   |   |   | 2 | 0.90 | 100.00 |
|   |   |   | 3 | 0.90 | 100.00 |
| 1k | 5 | 4.48 | 1 | 4.48 | 100.00 |
|   |   |   | 2 | 4.49 | 100.00 |
|   |   |   | 3 | 4.48 | 100.00 |
| 1k | 10 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.98 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 1k | 20 | 17.92 | 1 | 17.94 | 100.00 |
|   |   |   | 2 | 17.94 | 100.00 |
|   |   |   | 3 | 17.95 | 100.00 |
| 1k | 50 | 44.80 | 1 | 44.85 | 100.00 |
|   |   |   | 2 | 44.87 | 100.00 |
|   |   |   | 3 | 44.89 | 100.00 |
| 1k | 100 | 89.60 | 1 | 89.69 | 100.00 |
|   |   |   | 2 | 89.73 | 100.00 |
|   |   |   | 3 | 89.78 | 100.00 |
| 10k | 1 | 8.96 | 1 | 8.98 | 100.00 |
|   |   |   | 2 | 8.97 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 10k | 5 | 44.80 | 1 | 44.86 | 100.00 |
|   |   |   | 2 | 44.89 | 100.00 |
|   |   |   | 3 | 44.84 | 100.00 |
| 10k | 10 | 89.60 | 1 | 89.69 | 100.00 |
|   |   |   | 2 | 89.74 | 100.00 |
|   |   |   | 3 | 89.81 | 100.00 |
| 10k | 20 | 179.20 | 1 | 179.39 | 100.00 |
|   |   |   | 2 | 179.40 | 100.00 |
|   |   |   | 3 | 179.50 | 100.00 |
| 10k | 50 | 448.00 | 1 | 448.29 | 100.00 |
|   |   |   | 2 | 448.54 | 100.00 |
|   |   |   | 3 | 448.85 | 100.00 |
| 10k | 100 | 896.00 | 1 | 896.86 | 100.00 |
|   |   |   | 2 | 896.96 | 100.00 |
|   |   |   | 3 | 897.52 | 100.00 |
| 100k | 1 | 89.60 | 1 | 89.77 | 100.00 |
|   |   |   | 2 | 89.78 | 100.00 |
|   |   |   | 3 | 89.82 | 100.00 |
| 100k | 5 | 448.00 | 1 | 448.50 | 100.00 |
|   |   |   | 2 | 448.79 | 100.00 |
|   |   |   | 3 | 448.63 | 100.00 |
| 100k | 10 | 896.00 | 1 | 896.73 | 100.00 |
|   |   |   | 2 | 897.11 | 100.00 |
|   |   |   | 3 | 897.72 | 100.00 |
| 100k | 20 | 1792.00 | 1 | 1793.26 | 100.00 |
|   |   |   | 2 | 1793.49 | 100.00 |
|   |   |   | 3 | 1795.77 | 100.00 |
| 100k | 50 | 4480.00 | 1 | 4486.33 | 100.00 |
|   |   |   | 2 | 4486.66 | 100.00 |
|   |   |   | 3 | 4489.44 | 100.00 |
| 100k | 100 | 8960.00 | 1 | 5028.07 | 60.85 |
|   |   |   | 2 | 4121.43 | 51.33 |
|   |   |   | 3 | 4657.56 | 52.90 |


![](p4p_chain_gw.png)

In this case, we also check the load on the gateway `epics-gw-lab-02.cslab.esss.lu.se` because all trafic goes through it as well.
![](p4p_chain_gw_gw02.png)


## Comparison
The gateway based on p4p v3 gave a better transmission for high load than the one based on p4p v4, but the difference might depend on the load of the CSLab cluster at the time of running the tests.

Chaining 2 p4p v4 based gateways seems to have only a minor effect on transmission.

The following plot shows a comparison of the transmission for the different scenarios:
![](results.png)

The delay of a `pvget` command to acquire a scalar PV (double) was also monitored for the case with no load, with the highest load that still got 100% transmission, and for the highest load that saturated the gateway. 

For each case, the pvget command is executed 5 times and the table below shows the average and standard deviation:
| Gateway | No load | 4480 Mbps | 8960 Mbps |
| --- | --- | --- | --- |
| gw-01 | 114±4 ms | 143±21 ms | 235±14 ms | 
| gw-02 | 115±4 ms | 160±15 ms | 450±21 ms | 
| gw-03 | 118±6 ms | 178±22 ms | 898±764 ms | 

For the case with 4480 Mbps, the delay slightly increases in the 4 cases, but only by a few tens of milliseconds. When the load is too high and the gateway starts to drop values, then the delay increases significantly, especially for the case with the chained gateways.

NOTE: for the CA gateway used in production, which is setup as a chained gateway, the delay increases with significantly lower load. Monitoring a single PV with 300 000 points updating at 1 Hz (19.2 Mbps) is enough to increase the delay from ~100 ms to 2.1±0.74 s (average and standard deviation over 5 measures).