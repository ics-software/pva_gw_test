Started: 2021-11-20 00:18:14

Data rates tested:

| # points | Min. Data Rate [Mbps] | Max. Data Rate [Mbps] 
| --- | --- | --- |
| 1k | 0.90 | 89.60 |
| 10k | 8.96 | 896.00 |
| 100k | 89.60 | 8960.00 |

The test will take 0:27:0 s.


| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |
| 1k | 1 | 0.90 | 1 | 0.90 | 100.00 |
|   |   |   | 2 | 0.90 | 100.00 |
|   |   |   | 3 | 0.90 | 100.00 |
| 1k | 5 | 4.48 | 1 | 4.48 | 100.00 |
|   |   |   | 2 | 4.49 | 100.00 |
|   |   |   | 3 | 4.48 | 100.00 |
| 1k | 10 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.98 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 1k | 20 | 17.92 | 1 | 17.94 | 100.00 |
|   |   |   | 2 | 17.94 | 100.00 |
|   |   |   | 3 | 17.95 | 100.00 |
| 1k | 50 | 44.80 | 1 | 44.85 | 100.00 |
|   |   |   | 2 | 44.87 | 100.00 |
|   |   |   | 3 | 44.89 | 100.00 |
| 1k | 100 | 89.60 | 1 | 89.69 | 100.00 |
|   |   |   | 2 | 89.73 | 100.00 |
|   |   |   | 3 | 89.78 | 100.00 |
| 10k | 1 | 8.96 | 1 | 8.98 | 100.00 |
|   |   |   | 2 | 8.97 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 10k | 5 | 44.80 | 1 | 44.86 | 100.00 |
|   |   |   | 2 | 44.89 | 100.00 |
|   |   |   | 3 | 44.84 | 100.00 |
| 10k | 10 | 89.60 | 1 | 89.69 | 100.00 |
|   |   |   | 2 | 89.74 | 100.00 |
|   |   |   | 3 | 89.81 | 100.00 |
| 10k | 20 | 179.20 | 1 | 179.39 | 100.00 |
|   |   |   | 2 | 179.40 | 100.00 |
|   |   |   | 3 | 179.50 | 100.00 |
| 10k | 50 | 448.00 | 1 | 448.29 | 100.00 |
|   |   |   | 2 | 448.54 | 100.00 |
|   |   |   | 3 | 448.85 | 100.00 |
| 10k | 100 | 896.00 | 1 | 896.86 | 100.00 |
|   |   |   | 2 | 896.96 | 100.00 |
|   |   |   | 3 | 897.52 | 100.00 |
| 100k | 1 | 89.60 | 1 | 89.77 | 100.00 |
|   |   |   | 2 | 89.78 | 100.00 |
|   |   |   | 3 | 89.82 | 100.00 |
| 100k | 5 | 448.00 | 1 | 448.50 | 100.00 |
|   |   |   | 2 | 448.79 | 100.00 |
|   |   |   | 3 | 448.63 | 100.00 |
| 100k | 10 | 896.00 | 1 | 896.73 | 100.00 |
|   |   |   | 2 | 897.11 | 100.00 |
|   |   |   | 3 | 897.72 | 100.00 |
| 100k | 20 | 1792.00 | 1 | 1793.26 | 100.00 |
|   |   |   | 2 | 1793.49 | 100.00 |
|   |   |   | 3 | 1795.77 | 100.00 |
| 100k | 50 | 4480.00 | 1 | 4486.33 | 100.00 |
|   |   |   | 2 | 4486.66 | 100.00 |
|   |   |   | 3 | 4489.44 | 100.00 |
| 100k | 100 | 8960.00 | 1 | 5028.07 | 60.85 |
|   |   |   | 2 | 4121.43 | 51.33 |
|   |   |   | 3 | 4657.56 | 52.90 |
