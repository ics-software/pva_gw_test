2021-11-19T23:09:38.829 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:40207, disconnecting...
2021-11-19T23:09:38.832 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:39845, disconnecting...
2021-11-19T23:10:38.901 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:40207, disconnecting...
2021-11-19T23:14:39.261 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:40207, disconnecting...
2021-11-19T23:14:39.264 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:5075, disconnecting...
2021-11-19T23:14:39.265 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:39845, disconnecting...
2021-11-19T23:18:39.410 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:35005, disconnecting...
2021-11-19T23:21:39.563 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:5075, disconnecting...
2021-11-19T23:21:39.565 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:40207, disconnecting...
2021-11-19T23:21:39.567 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:39845, disconnecting...
2021-11-19T23:21:39.572 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:35005, disconnecting...
2021-11-19T23:23:09.774 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:39845, disconnecting...
2021-11-19T23:23:09.792 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:5075, disconnecting...
2021-11-19T23:23:39.837 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:40207, disconnecting...
2021-11-19T23:23:39.839 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:5075, disconnecting...
2021-11-19T23:23:39.846 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:39845, disconnecting...
2021-11-19T23:23:39.860 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.4.233:35005, disconnecting...
Started: 2021-11-19 22:57:07

Data rates tested:

| # points | Min. Data Rate [Mbps] | Max. Data Rate [Mbps] 
| --- | --- | --- |
| 1k | 0.90 | 89.60 |
| 10k | 8.96 | 896.00 |
| 100k | 89.60 | 8960.00 |

The test will take 0:27:0 s.


| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |
| 1k | 1 | 0.90 | 1 | 0.90 | 100.00 |
|   |   |   | 2 | 0.90 | 100.00 |
|   |   |   | 3 | 0.90 | 100.00 |
| 1k | 5 | 4.48 | 1 | 4.49 | 100.00 |
|   |   |   | 2 | 4.49 | 100.00 |
|   |   |   | 3 | 4.49 | 100.00 |
| 1k | 10 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.97 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 1k | 20 | 17.92 | 1 | 17.94 | 100.00 |
|   |   |   | 2 | 17.94 | 100.00 |
|   |   |   | 3 | 17.95 | 100.00 |
| 1k | 50 | 44.80 | 1 | 44.89 | 100.00 |
|   |   |   | 2 | 44.88 | 100.00 |
|   |   |   | 3 | 44.85 | 100.00 |
| 1k | 100 | 89.60 | 1 | 89.69 | 100.00 |
|   |   |   | 2 | 89.74 | 100.00 |
|   |   |   | 3 | 89.71 | 100.00 |
| 10k | 1 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.97 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 10k | 5 | 44.80 | 1 | 44.84 | 100.00 |
|   |   |   | 2 | 44.84 | 100.00 |
|   |   |   | 3 | 44.85 | 100.00 |
| 10k | 10 | 89.60 | 1 | 89.72 | 100.00 |
|   |   |   | 2 | 89.69 | 100.00 |
|   |   |   | 3 | 89.69 | 100.00 |
| 10k | 20 | 179.20 | 1 | 179.36 | 100.00 |
|   |   |   | 2 | 179.40 | 100.00 |
|   |   |   | 3 | 179.39 | 100.00 |
| 10k | 50 | 448.00 | 1 | 448.47 | 100.00 |
|   |   |   | 2 | 448.47 | 100.00 |
|   |   |   | 3 | 448.47 | 100.00 |
| 10k | 100 | 896.00 | 1 | 897.35 | 100.00 |
|   |   |   | 2 | 896.85 | 100.00 |
|   |   |   | 3 | 897.26 | 100.00 |
| 100k | 1 | 89.60 | 1 | 89.68 | 100.00 |
|   |   |   | 2 | 89.69 | 100.00 |
|   |   |   | 3 | 89.69 | 100.00 |
| 100k | 5 | 448.00 | 1 | 448.59 | 100.00 |
|   |   |   | 2 | 448.43 | 100.00 |
|   |   |   | 3 | 448.40 | 100.00 |
| 100k | 10 | 896.00 | 1 | 896.91 | 100.00 |
|   |   |   | 2 | 896.95 | 100.00 |
|   |   |   | 3 | 896.73 | 100.00 |
| 100k | 20 | 1792.00 | 1 | 1795.61 | 100.00 |
|   |   |   | 2 | 1794.47 | 100.00 |
|   |   |   | 3 | 1793.35 | 100.00 |
| 100k | 50 | 4480.00 | 1 | 4483.26 | 100.00 |
|   |   |   | 2 | 4484.15 | 100.00 |
|   |   |   | 3 | 4484.07 | 100.00 |
| 100k | 100 | 8960.00 | 1 | 8971.56 | 100.00 |
|   |   |   | 2 | 8971.91 | 100.00 |
|   |   |   | 3 | 8972.45 | 100.00 |
2021-11-19T23:33:10.846 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.9.11:5075, disconnecting...
2021-11-19T23:40:11.357 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.9.11:5075, disconnecting...
Orphaned server message 1 : bad channel id
Orphaned server message 1 : bad channel id
Orphaned server message 1 : bad channel id
Orphaned server message 1 : bad channel id
Orphaned server message 1 : bad channel id
Orphaned server message 1 : bad channel id
Orphaned server message 1 : bad channel id
2021-11-19T23:47:11.939 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.9.11:5075, disconnecting...
2021-11-19T23:48:12.020 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.9.11:5075, disconnecting...
2021-11-19T23:49:12.112 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.9.11:5075, disconnecting...
2021-11-19T23:49:42.165 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.9.11:5075, disconnecting...
2021-11-19T23:50:12.245 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.9.11:5075, disconnecting...
2021-11-19T23:50:42.320 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.9.11:5075, disconnecting...
2021-11-19T23:51:12.372 Protocol Violation: Not-a-first segmented message received in normal mode from the client at modules/pvAccess/src/remote/codec.cpp:239: 172.30.9.11:5075, disconnecting...
Started: 2021-11-19 23:24:10

Data rates tested:

| # points | Min. Data Rate [Mbps] | Max. Data Rate [Mbps] 
| --- | --- | --- |
| 1k | 0.90 | 89.60 |
| 10k | 8.96 | 896.00 |
| 100k | 89.60 | 8960.00 |

The test will take 0:27:0 s.


| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |
| 1k | 1 | 0.90 | 1 | 0.90 | 100.00 |
|   |   |   | 2 | 0.90 | 100.00 |
|   |   |   | 3 | 0.90 | 100.00 |
| 1k | 5 | 4.48 | 1 | 4.49 | 100.00 |
|   |   |   | 2 | 4.48 | 100.00 |
|   |   |   | 3 | 4.49 | 100.00 |
| 1k | 10 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.98 | 100.00 |
|   |   |   | 3 | 8.98 | 100.00 |
| 1k | 20 | 17.92 | 1 | 17.94 | 100.00 |
|   |   |   | 2 | 17.93 | 100.00 |
|   |   |   | 3 | 17.94 | 100.00 |
| 1k | 50 | 44.80 | 1 | 44.86 | 100.00 |
|   |   |   | 2 | 44.86 | 100.00 |
|   |   |   | 3 | 44.89 | 100.00 |
| 1k | 100 | 89.60 | 1 | 89.72 | 100.00 |
|   |   |   | 2 | 89.84 | 100.00 |
|   |   |   | 3 | 89.69 | 100.00 |
| 10k | 1 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.98 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 10k | 5 | 44.80 | 1 | 44.88 | 100.00 |
|   |   |   | 2 | 44.85 | 100.00 |
|   |   |   | 3 | 44.87 | 100.00 |
| 10k | 10 | 89.60 | 1 | 89.72 | 100.00 |
|   |   |   | 2 | 89.78 | 100.00 |
|   |   |   | 3 | 89.71 | 100.00 |
| 10k | 20 | 179.20 | 1 | 179.31 | 100.00 |
|   |   |   | 2 | 179.47 | 100.00 |
|   |   |   | 3 | 179.40 | 100.00 |
| 10k | 50 | 448.00 | 1 | 449.02 | 100.00 |
|   |   |   | 2 | 448.87 | 100.00 |
|   |   |   | 3 | 449.09 | 100.00 |
| 10k | 100 | 896.00 | 1 | 896.76 | 100.00 |
|   |   |   | 2 | 897.34 | 100.00 |
|   |   |   | 3 | 897.30 | 100.00 |
| 100k | 1 | 89.60 | 1 | 89.73 | 100.00 |
|   |   |   | 2 | 89.69 | 100.00 |
|   |   |   | 3 | 89.71 | 100.00 |
| 100k | 5 | 448.00 | 1 | 448.74 | 100.00 |
|   |   |   | 2 | 448.74 | 100.00 |
|   |   |   | 3 | 448.85 | 100.00 |
| 100k | 10 | 896.00 | 1 | 896.93 | 100.00 |
|   |   |   | 2 | 896.86 | 100.00 |
|   |   |   | 3 | 896.98 | 100.00 |
| 100k | 20 | 1792.00 | 1 | 1794.26 | 100.00 |
|   |   |   | 2 | 1794.63 | 100.00 |
|   |   |   | 3 | 1794.62 | 100.00 |
| 100k | 50 | 4480.00 | 1 | 4490.27 | 100.00 |
|   |   |   | 2 | 4500.65 | 100.00 |
|   |   |   | 3 | 4496.35 | 100.00 |
| 100k | 100 | 8960.00 | 1 | 5853.99 | 67.57 |
|   |   |   | 2 | 6222.48 | 70.96 |
|   |   |   | 3 | 6217.93 | 70.71 |
2021-11-20T00:00:13.284 unprocessed read buffer from client at modules/pvAccess/src/remote/codec.cpp:324: 172.30.9.12:5075, disconnecting...
2021-11-20T00:02:13.429 unprocessed read buffer from client at modules/pvAccess/src/remote/codec.cpp:324: 172.30.9.12:5075, disconnecting...
2021-11-20T00:11:14.011 unprocessed read buffer from client at modules/pvAccess/src/remote/codec.cpp:324: 172.30.9.12:5075, disconnecting...
2021-11-20T00:15:14.166 unprocessed read buffer from client at modules/pvAccess/src/remote/codec.cpp:324: 172.30.9.12:5075, disconnecting...
2021-11-20T00:17:14.365 unprocessed read buffer from client at modules/pvAccess/src/remote/codec.cpp:324: 172.30.9.12:5075, disconnecting...
2021-11-20T00:17:44.532 unprocessed read buffer from client at modules/pvAccess/src/remote/codec.cpp:324: 172.30.9.12:5075, disconnecting...
Started: 2021-11-19 23:51:12

Data rates tested:

| # points | Min. Data Rate [Mbps] | Max. Data Rate [Mbps] 
| --- | --- | --- |
| 1k | 0.90 | 89.60 |
| 10k | 8.96 | 896.00 |
| 100k | 89.60 | 8960.00 |

The test will take 0:27:0 s.


| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |
| 1k | 1 | 0.90 | 1 | 0.90 | 100.00 |
|   |   |   | 2 | 0.90 | 100.00 |
|   |   |   | 3 | 0.90 | 100.00 |
| 1k | 5 | 4.48 | 1 | 4.48 | 100.00 |
|   |   |   | 2 | 4.49 | 100.00 |
|   |   |   | 3 | 4.48 | 100.00 |
| 1k | 10 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.97 | 100.00 |
|   |   |   | 3 | 8.98 | 100.00 |
| 1k | 20 | 17.92 | 1 | 17.95 | 100.00 |
|   |   |   | 2 | 17.94 | 100.00 |
|   |   |   | 3 | 17.96 | 100.00 |
| 1k | 50 | 44.80 | 1 | 44.88 | 100.00 |
|   |   |   | 2 | 44.87 | 100.00 |
|   |   |   | 3 | 44.90 | 100.00 |
| 1k | 100 | 89.60 | 1 | 89.74 | 100.00 |
|   |   |   | 2 | 89.72 | 100.00 |
|   |   |   | 3 | 89.86 | 100.00 |
| 10k | 1 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.97 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 10k | 5 | 44.80 | 1 | 44.84 | 100.00 |
|   |   |   | 2 | 44.89 | 100.00 |
|   |   |   | 3 | 44.84 | 100.00 |
| 10k | 10 | 89.60 | 1 | 89.73 | 100.00 |
|   |   |   | 2 | 89.68 | 100.00 |
|   |   |   | 3 | 89.68 | 100.00 |
| 10k | 20 | 179.20 | 1 | 179.43 | 100.00 |
|   |   |   | 2 | 179.38 | 100.00 |
|   |   |   | 3 | 179.36 | 100.00 |
| 10k | 50 | 448.00 | 1 | 448.50 | 100.00 |
|   |   |   | 2 | 448.43 | 100.00 |
|   |   |   | 3 | 448.48 | 100.00 |
| 10k | 100 | 896.00 | 1 | 897.07 | 100.00 |
|   |   |   | 2 | 897.15 | 100.00 |
|   |   |   | 3 | 897.53 | 100.00 |
| 100k | 1 | 89.60 | 1 | 89.69 | 100.00 |
|   |   |   | 2 | 89.85 | 100.00 |
|   |   |   | 3 | 89.68 | 100.00 |
| 100k | 5 | 448.00 | 1 | 448.32 | 100.00 |
|   |   |   | 2 | 448.90 | 100.00 |
|   |   |   | 3 | 448.46 | 100.00 |
| 100k | 10 | 896.00 | 1 | 897.12 | 100.00 |
|   |   |   | 2 | 896.73 | 100.00 |
|   |   |   | 3 | 897.67 | 100.00 |
| 100k | 20 | 1792.00 | 1 | 1793.89 | 100.00 |
|   |   |   | 2 | 1793.09 | 100.00 |
|   |   |   | 3 | 1794.85 | 100.00 |
| 100k | 50 | 4480.00 | 1 | 4484.06 | 100.00 |
|   |   |   | 2 | 4485.80 | 100.00 |
|   |   |   | 3 | 4493.31 | 100.00 |
| 100k | 100 | 8960.00 | 1 | 5081.53 | 58.77 |
|   |   |   | 2 | 4717.88 | 53.96 |
|   |   |   | 3 | 5609.51 | 63.56 |
2021-11-20T00:30:15.731 unprocessed read buffer from client at modules/pvAccess/src/remote/codec.cpp:324: 172.30.9.13:5075, disconnecting...
2021-11-20T00:32:15.878 unprocessed read buffer from client at modules/pvAccess/src/remote/codec.cpp:324: 172.30.9.13:5075, disconnecting...
2021-11-20T00:35:16.171 unprocessed read buffer from client at modules/pvAccess/src/remote/codec.cpp:324: 172.30.9.13:5075, disconnecting...
2021-11-20T00:35:46.231 unprocessed read buffer from client at modules/pvAccess/src/remote/codec.cpp:324: 172.30.9.13:5075, disconnecting...
2021-11-20T00:39:16.462 unprocessed read buffer from client at modules/pvAccess/src/remote/codec.cpp:324: 172.30.9.13:5075, disconnecting...
2021-11-20T00:44:16.925 unprocessed read buffer from client at modules/pvAccess/src/remote/codec.cpp:324: 172.30.9.13:5075, disconnecting...
Started: 2021-11-20 00:18:14

Data rates tested:

| # points | Min. Data Rate [Mbps] | Max. Data Rate [Mbps] 
| --- | --- | --- |
| 1k | 0.90 | 89.60 |
| 10k | 8.96 | 896.00 |
| 100k | 89.60 | 8960.00 |

The test will take 0:27:0 s.


| # points | # PVs | Data Rate [Mbps] | # run | Rec. data rate [Mbps] | Transmission [%] |
| --- | --- | --- | --- | --- | --- |
| 1k | 1 | 0.90 | 1 | 0.90 | 100.00 |
|   |   |   | 2 | 0.90 | 100.00 |
|   |   |   | 3 | 0.90 | 100.00 |
| 1k | 5 | 4.48 | 1 | 4.48 | 100.00 |
|   |   |   | 2 | 4.49 | 100.00 |
|   |   |   | 3 | 4.48 | 100.00 |
| 1k | 10 | 8.96 | 1 | 8.97 | 100.00 |
|   |   |   | 2 | 8.98 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 1k | 20 | 17.92 | 1 | 17.94 | 100.00 |
|   |   |   | 2 | 17.94 | 100.00 |
|   |   |   | 3 | 17.95 | 100.00 |
| 1k | 50 | 44.80 | 1 | 44.85 | 100.00 |
|   |   |   | 2 | 44.87 | 100.00 |
|   |   |   | 3 | 44.89 | 100.00 |
| 1k | 100 | 89.60 | 1 | 89.69 | 100.00 |
|   |   |   | 2 | 89.73 | 100.00 |
|   |   |   | 3 | 89.78 | 100.00 |
| 10k | 1 | 8.96 | 1 | 8.98 | 100.00 |
|   |   |   | 2 | 8.97 | 100.00 |
|   |   |   | 3 | 8.97 | 100.00 |
| 10k | 5 | 44.80 | 1 | 44.86 | 100.00 |
|   |   |   | 2 | 44.89 | 100.00 |
|   |   |   | 3 | 44.84 | 100.00 |
| 10k | 10 | 89.60 | 1 | 89.69 | 100.00 |
|   |   |   | 2 | 89.74 | 100.00 |
|   |   |   | 3 | 89.81 | 100.00 |
| 10k | 20 | 179.20 | 1 | 179.39 | 100.00 |
|   |   |   | 2 | 179.40 | 100.00 |
|   |   |   | 3 | 179.50 | 100.00 |
| 10k | 50 | 448.00 | 1 | 448.29 | 100.00 |
|   |   |   | 2 | 448.54 | 100.00 |
|   |   |   | 3 | 448.85 | 100.00 |
| 10k | 100 | 896.00 | 1 | 896.86 | 100.00 |
|   |   |   | 2 | 896.96 | 100.00 |
|   |   |   | 3 | 897.52 | 100.00 |
| 100k | 1 | 89.60 | 1 | 89.77 | 100.00 |
|   |   |   | 2 | 89.78 | 100.00 |
|   |   |   | 3 | 89.82 | 100.00 |
| 100k | 5 | 448.00 | 1 | 448.50 | 100.00 |
|   |   |   | 2 | 448.79 | 100.00 |
|   |   |   | 3 | 448.63 | 100.00 |
| 100k | 10 | 896.00 | 1 | 896.73 | 100.00 |
|   |   |   | 2 | 897.11 | 100.00 |
|   |   |   | 3 | 897.72 | 100.00 |
| 100k | 20 | 1792.00 | 1 | 1793.26 | 100.00 |
|   |   |   | 2 | 1793.49 | 100.00 |
|   |   |   | 3 | 1795.77 | 100.00 |
| 100k | 50 | 4480.00 | 1 | 4486.33 | 100.00 |
|   |   |   | 2 | 4486.66 | 100.00 |
|   |   |   | 3 | 4489.44 | 100.00 |
| 100k | 100 | 8960.00 | 1 | 5028.07 | 60.85 |
|   |   |   | 2 | 4121.43 | 51.33 |
|   |   |   | 3 | 4657.56 | 52.90 |
