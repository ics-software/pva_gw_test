#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This script runs different tests using client_loop.py,
# and each test can use different EPICS settings (e.g., gateway).


import os
import subprocess

import numpy as np
import pylab as plt


def read_datarate(filename):
    f = open(filename+'.txt', 'r')
    data = dict()
    for line in f:
        dr, tx = line.split('\t')
        tx = tx.replace('nan', '0')
        data[float(dr)] = eval(tx)

    return data


def plot_errorbar(x, y, label=''):
    y_avg = np.mean(y, 1)
    y_min_delta = y_avg - np.min(y, 1)
    y_max_delta = np.max(y, 1) - y_avg

    y_avg = y_avg[x.argsort()]
    y_min_delta = y_min_delta[x.argsort()]
    y_max_delta = y_max_delta[x.argsort()]
    x.sort()

    plt.errorbar(x, y_avg, yerr=[y_min_delta,
                                 y_max_delta], fmt='.-', capsize=3, label=label)


acq_time = '30'  # seconds
n_runs = '3'

# First case without gateways
os.environ['EPICS_PVA_ADDR_LIST'] = ''
os.environ['EPICS_PVA_AUTO_ADDR_LIST'] = 'YES'

subprocess.call(['python', 'client_loop.py', acq_time, n_runs, 'no_gw'])


# Gateway p4p v3.4.2
os.environ['EPICS_PVA_ADDR_LIST'] = 'epics-gw-lab-01.cslab.esss.lu.se'
os.environ['EPICS_PVA_AUTO_ADDR_LIST'] = 'NO'

subprocess.call(['python', 'client_loop.py', acq_time, n_runs, 'gw1'])


# Gateway p4p v4.0.2a
os.environ['EPICS_PVA_ADDR_LIST'] = 'epics-gw-lab-02.cslab.esss.lu.se'
os.environ['EPICS_PVA_AUTO_ADDR_LIST'] = 'NO'

subprocess.call(['python', 'client_loop.py', acq_time, n_runs, 'gw2'])


# Chained gateway
os.environ['EPICS_PVA_ADDR_LIST'] = 'epics-gw-lab-03.cslab.esss.lu.se'
os.environ['EPICS_PVA_AUTO_ADDR_LIST'] = 'NO'

subprocess.call(['python', 'client_loop.py', acq_time, n_runs, 'gw3'])


plt.figure()
data = read_datarate('no_gw')
plot_errorbar(np.array(list(data.keys()))*1e-6,
              list(data.values()), label='No gateway')

data = read_datarate('gw1')
plot_errorbar(np.array(list(data.keys()))*1e-6,
              list(data.values()), label='Gateway p4p v3.4.2')

data = read_datarate('gw2')
plot_errorbar(np.array(list(data.keys()))*1e-6,
              list(data.values()), label='Gateway p4p v4.0.2a')

data = read_datarate('gw3')
plot_errorbar(np.array(list(data.keys()))*1e-6,
              list(data.values()), label='Chained gateways')

plt.xlabel('Data rate at IOC [Mbps]')
plt.ylabel('Transmission [%]')
plt.legend(loc=0)

plt.savefig('results.png')
